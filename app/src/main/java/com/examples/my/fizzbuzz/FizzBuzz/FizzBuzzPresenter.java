package com.examples.my.fizzbuzz.FizzBuzz;

/**
 * Created by stevanburrows on 20/09/2017.
 */

public class FizzBuzzPresenter {

    private FizzBuzzView mContract;

    /**
     * Public constructor
     */
    public FizzBuzzPresenter(FizzBuzzView contract) {
        this.mContract = contract;
    }

    public void outputFizzBuzz() {

        for (int i=1; i<=100; i++) {
            mContract.outputToText(getOutput(i));
        }
    }

    public String getOutput(int number) {

        boolean isFizz = isMultiple(number, 3);
        boolean isBuzz = isMultiple(number, 5);

        if (isFizz)
            if (isBuzz)
                return "FizzBuzz";
                else return "Fizz";
        else if (isBuzz)
            return "Buzz";

        return String.valueOf(number);
    }

    private boolean isMultiple(int x, int y) {

        if (x % y == 0)
            return true;

        return false;
    }
}

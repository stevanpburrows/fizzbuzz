package com.examples.my.fizzbuzz.FizzBuzz;

/**
 * Created by stevanburrows on 20/09/2017.
 */

public interface FizzBuzzView {

    void outputToText(String output);
}

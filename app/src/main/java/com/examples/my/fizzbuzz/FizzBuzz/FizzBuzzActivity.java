package com.examples.my.fizzbuzz.FizzBuzz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.examples.my.fizzbuzz.R;

public class FizzBuzzActivity extends AppCompatActivity implements FizzBuzzView {

    private TextView mOutputText;
    private FizzBuzzPresenter mFizzBuzzPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fizz_buzz);

        initialisePresenter();
        initialiseViews();
    }

    private void initialisePresenter() {
        mFizzBuzzPresenter = new FizzBuzzPresenter(this);
    }

    private void initialiseViews() {

        mOutputText = (TextView) findViewById(R.id.output_text);

        mFizzBuzzPresenter.outputFizzBuzz();
    }


    @Override
    public void outputToText(String output) {

        mOutputText.setText(mOutputText.getText().toString() + " \n" + output);
    }
}

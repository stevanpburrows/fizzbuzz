package com.examples.my.fizzbuzz.FizzBuzz;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by stevanburrows on 20/09/2017.
 */

@RunWith(JUnit4.class)
public class FizzBuzzPresenterTest {

    @Test
    public void failingMultiplesOfThreeButNotFiveAreFizz() {
        FizzBuzzView contract = mock(FizzBuzzView.class);
        FizzBuzzPresenter presenter = new FizzBuzzPresenter(contract);
        for (int i = 1; i <= 100; i++) {
            if ((i % 3 == 0) && !(i % 5 == 0)) {
                assertEquals("Buzz", presenter.getOutput(i));
            }
        }
    }

    @Test
    public void multiplesOfThreeButNotFiveAreFizz() {
        FizzBuzzView contract = mock(FizzBuzzView.class);
        FizzBuzzPresenter presenter = new FizzBuzzPresenter(contract);
        for (int i = 1; i <= 100; i++) {
            if ((i % 3 == 0) && !(i % 5 == 0)) {
                assertEquals("Fizz", presenter.getOutput(i));
            }
        }
    }

    @Test
    public void multiplesOfFiveButNotThreeAreBuzz() {
        FizzBuzzView contract = mock(FizzBuzzView.class);
        FizzBuzzPresenter presenter = new FizzBuzzPresenter(contract);
        for (int i = 1; i <= 100; i++) {
            if (!(i % 3 == 0) && (i % 5 == 0)) {
                assertEquals("Buzz", presenter.getOutput(i));
            }
        }
    }

    @Test
    public void multiplesOfThreeAndFiveAreFizzBuzz() {
        FizzBuzzView contract = mock(FizzBuzzView.class);
        FizzBuzzPresenter presenter = new FizzBuzzPresenter(contract);
        for (int i = 1; i <= 100; i++) {
            if ((i % 3 == 0) && (i % 5 == 0)) {
                assertEquals("FizzBuzz", presenter.getOutput(i));
            }
        }
    }
}